<?php
class DatabaseConnection{
    private $servername = "localhost";
    private $username = "root";
    private $password = "";
    private $database ="test";
    public $connection;
    
    public function getConnection(){
        $this->connection = null;  
        try{
            $this->connection =  new PDO("mysql:host=" . $this->servername . ";dbname=" . $this->database, $this->username, $this->password);
            $this->connection->exec("set names utf8");
        }catch(PDOException $exception){
            echo "Error: " . $exception->getMessage();
        }

        return $this->connection;
    }
}
