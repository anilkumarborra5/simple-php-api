<!DOCTYPE html>
<html>
<head>
<style>
#header {
  font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
  border-collapse: collapse;
  width: 100%;
}

#header td, #header th {
  border: 1px solid #ddd;
  padding: 8px;
}

#header tr:nth-child(even){background-color: #f2f2f2;}

#header tr:hover {background-color: #ddd;}

#header th {
  padding-top: 12px;
  padding-bottom: 12px;
  text-align: left;
  background-color:rgba(255, 141, 0, 0.8);
  color: white;
}
</style>
</head>
<body>
<h1>Dog API Data</h1>
<table id="header">
  <tr>
    <th>S.No</th>
    <th>Category</th>
    <th>Breed</th>
  </tr>
  <?php 
  if($dogs_fetch_data){ 
    foreach($dogs_fetch_data  as $key=>$value) {
        echo "<tr><td>".($key+1)."</td><td>".$value['category_name']."</td><td>".$value['breed_name']."</td></tr>";
    }
  } ?>
</table>

</body>
</html>
