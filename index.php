<?php 
require_once './config/DatabaseConnection.php';
require_once './module/dogs_model.php';

$api_url ="https://dog.ceo/api/breeds/list/all";
$dogs_details =(array) json_decode(file_get_contents($api_url),true);

if(is_array($dogs_details)){
    $db = new DatabaseConnection();
    $connection = $db->getConnection();
    $update_dogs_details = new DogsModel($connection);
    foreach($dogs_details['message'] as $category=>$breeds){
       $category_id = $update_dogs_details->set_dogs_category($category);
        if(is_array($breeds) && count($breeds)>=1){
            foreach($breeds as $breed){
                $update_dogs_details->set_dogs_breeds($category_id,$breed);
            }
        }
    }
}
$dogs_fetch_data = $update_dogs_details->get_dogs_details();
require_once './view/dogs_table.php';

// echo "<pre>";print_r($dogs_details);

// $client = curl_init($api_url);
// curl_setopt($client,CURLOPT_RETURNTRANSFER,true);
// $data = curl_exec($cleint);
// $result = json_decode($data);
// print_r($result);

