<?php
class DogsModel{
    private $db;
    public function __construct($connection){
        $this->db = $connection;
    }
    public function get_dogs_details(){
        $result  = $this->db->query("SELECT dc.category_id, dc.category_name,db.breed_name FROM dogs_category dc LEFT JOIN dog_breeds db On db.category_id = dc.category_id ORDER BY `dc`.`category_id` ASC",PDO::FETCH_ASSOC);
        
            return $result;
        
    }
    public function set_dogs_category($category=NULL){
        $sql_query = $this->db->prepare("INSERT INTO `dogs_category`(`category_name`) VALUES (?)");
        $sql_query->execute(array($category));
        return $this->db->lastInsertId(); 
    }
    public function set_dogs_breeds($category_id=0,$breed=NULL){
        $sql_query = $this->db->prepare("INSERT INTO `dog_breeds`(`category_id`,`breed_name`) VALUES (?,?)");
        $sql_query->execute(array($category_id,$breed));
        if($this->db->lastInsertId()>=0)
        return true; 
    }
}
    